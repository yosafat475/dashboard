<?php

$curl = curl_init();
curl_setopt($curl,CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result,true);

$global = $result['Global'];
$temp_NewConfirmed = (string)$global['NewConfirmed'];
$TotalConfirmed =(string)$global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$TotalDeaths =(string)$global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$TotalRecovered =(string)$global['TotalRecovered'];
$countries =$result['Countries'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Data Covid-19</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <!-- ================================================card ====================================-->
        <div class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand">Dashboard | Virus Covid-19</a>
        </div>
        <h1 id="judul" style="text-align: center; color: white;" >Data Covid-19 Se Dunia</h1>
        <!-- ==============================card ===========================-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div id="b1"  class="card" style="width: 18rem; ">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><h3>Total Kematian</h3> <p class="cardtgl"><?php echo date('d F Y'); ?></p></li>
                            <li class="cad"><h5 class="sa"><?php echo number_format($TotalDeaths); ?></h3></li>
                            <li class="cad"><?php echo number_format($temp_NewDeaths); ?> New Deaths</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="b2" class="card" style="width: 18rem;">
                        <ul class="list-group list-group-flush">
                    <li class="list-group-item"><h5>Total Dipulihkan</h5> <p class="cardtgl"><?php echo date('d F Y'); ?></p></li>
                    <li class="cad"><h3 class="sa"><?php echo number_format($TotalConfirmed); ?></h3></li>
                    <li class="cad"><?php echo number_format($temp_NewConfirmed ); ?> New Confirmed</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div id="b3" class="card" style="width: 18rem;">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><h5> Konfirmasi</h5> <p class="cardtgl"><?php echo date('d F Y'); ?></p></li>
                    <li class="cad"><h3 class="sa"><?php echo number_format($TotalRecovered); ?></h3></li>
                    <li class="cad"><?php echo number_format($temp_NewRecovered); ?> New Recovered</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- ==============================================================card =========================================-->
<div class="container-fluid">
    <div class="row">
    <div  class="col-md-6">
        <div id="bayangan" class="card">
        <div class="card-body">
                <h5 class="card-title">
                        <!-- ===================================chart ==================================-->
                    <div class="container-fluid"> 
                        <div class="row">
                            <h4 id="ab" >Data Seluruh Negara Dunia<p id="tgl">
                                <?php echo date('d F Y'); ?>
                            </p></h4>
                            <canvas id="myChart" style="height: 50%; width: 50%;"></canvas>
                        </div>
                    </div>
                </h5>
        </div>
        </div>
    </div>
    <div  class="col-md-3">
        <div id="bayangan" class="card">
        <div class="card-body">
            <div class="row">

                                <div class="card-header text-dark">
                                    <h5 id="try">Country</h5>
                                </div>
                                <div class="country border border-primary">
                                    <?php foreach($countries as $key=>$value):?>
                                    <ul  class="navbar-nav mr-auto">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle text-light bg-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?= $value['Country'];?>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <p class="dropdown-item">Total Confirmed: <?= $value['TotalConfirmed'];?> </p>
                                                <p class="dropdown-item">Total Deaths: <?= $value['TotalDeaths'];?> </p>
                                                <p class="dropdown-item" >Total Recovered: <?= $value['TotalRecovered'];?> </p>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php endforeach;?>
                                </div>
                            <!-- </div>
                        </div> -->
                </div>
            </div>
        </div>
        </div>
        
            <div class="col-md-3">
                <div  id="bayangan" class="card w-85">
                    <div class="card-body">
                                <h3 class="pro">Peringatan</h3>
                                <h4>Lindungi diri Anda dan orang lain di sekitar Anda dengan mengetahui fakta-fakta terkait virus ini dan mengambil langkah pencegahan yang sesuai.</h4> 

                    </div>
                </div>
                    <!-- <div  id="bayangan" class="card w-70">
                        <div class="card-body">
                        <h4 class="pro">Untuk mencegah penyebaran COVID-19:</h4>
                                <ul>
                                    <li>Cuci tangan Anda secara rutin</li>
                                    <li>Selalu jaga jarak aman dengan orang yang batuk atau bersin</li>
                                    <li>Kenakan masker jika pembatasan fisik tidak dimungkinkan</li>
                                    <li>Jangan sentuh mata, hidung, atau mulut Anda</li>
                                    <li>Jangan keluar rumah jika merasa tidak enak badan</li>
                                    
                                </ul>                     
                        </div>
                    </div> -->
            </div>
    </div>
</div>
<!-- =============penutup -->
<div id="adab" >
    <h2>COVID-19</h2>
    <P>jangan lupa selalu jaga kesehatan</P>
    <p>Belas kasihan mengikat kita satu sama lain. Bukan rasa kasihan, tetapi sebagai manusia yang belajar bagaimana mengubah penderitaan menjadi harapan untuk kedepan nya</p>
    <div id="by" class="container">
        <p>by:Yosafat  &copy;pro</p>
    </div>
</div>
<script>
        var ctx = document.getElementById('myChart').getContext('2d')

        var covid = $.ajax({
            url: "https://api.covid19api.com/summary",
            cache : false
        })

        .done(function (canvas) {
            
            
            function getContries(canvas) {
                var show_country=[];

                canvas.Countries.forEach(function(el) {
                    show_country.push(el.Country);
                })
                return show_country;
            }


            function getHealth(canvas) {
                var recovered=[];

                canvas.Countries.forEach(function(el) {
                    recovered.push(el.TotalRecovered)
                })
                return recovered;
            }   
            
            var color=[];
            function color_random(){
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            } 

            for ( var i in canvas.Countries) {
                color.push(color_random());
            }


            var myChart = new Chart(ctx,{
                type: 'bar',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: '# of Votes',
                        data: getHealth(canvas),
                        backgroundColor: color,
                        borderColor: color, 
                        borderWidth: 1
                    }]
                }
            })
        });
    </script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>